FROM openjdk:11.0.11-oraclelinux7
MAINTAINER Enrique Ramis
EXPOSE 8080
COPY target/prices-service-1.0.0.jar prices-service.jar
ENTRYPOINT ["java","-jar","/prices-service.jar"]

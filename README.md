# Capitole

## Comments/Considerations

- I will assume code of the brand,price list and product id are integers in database, because they are indexable foreign
  keys, and it's better for performance
- Dates will be stored as datetime, I won't consider working in different timezones. if it was a live application we
  should define the timezone of the server to convert date and times correctly before inserting in the database.
- Currency will be a 3 char column and will be of the domain of ISO 4217. see https://es.wikipedia.org/wiki/ISO_4217
- Numbers will have a maximum of 10 positions plus 5 decimal positions. This should be discussed with business to have a
  real approach of variation of prices and currencies and adapt it in database.
- Date format of the rest api is "2021-05-08T22:48:07"
- I built a simple crud for testing purposes, as it is not a live deployment endpoint I made it direct to the repository
  and used DAO in the interface, if it was a real crud controller we should have DTO's and a crudService to manage crud
  operations. As well, I added a parameter to prevent this controller to be shown in live environments.
- No method preference is indicated so I will build both GET and POST methods than contain same business service behind

## Libraries/modules used

The project has been build in java 11 with springboot2 version 2.4.5

I added a Dockerfile so it can be packaged in docker and deployed easier

- maven
- springboot web starter
- springboot test starter
- springboot jpa starter
- swagger 2
- h2
- lombok
- mockito
- docker

## Design

### Interface - prices endpoint

The Prices endpoint will give us applicable price for given brandId, productId and dateTime. it is a json endpoint that
allows two methods GET and POST, returning anb object of PriceResponseDTO type.

this service includes a swagger endpoint as self documentation and local testiong you can check it at:
http://localhost:8080/swagger-iu.html

|Method | EndPoint | Request | Result |
  |:---|:---|:----------------------|:---|
|GET | /prices | Path parameters brandId, productId, dateTime | 200 - PriceResponseDTO - if found applicable |
| |  |  | 404 - Not Found - if not found applicable |
| |  |  | 400 - Bad Request - if any of parameters are invalid |
| |  |  | 500 - Internal Server Error - Any unexpected error during the process |
|POST | /prices | PriceRequestDTO | 200 - PriceResponseDTO - if found applicable |
| |  |  | 404 - Not Found - if not found applicable |
| |  |  | 400 - Bad Request - if request body is invalid |
| |  |  | 500 - Internal Server Error - Any unexpected error during the process |

#### GET operation is described below.

**Method**: GET

**URL**: <server url>/prices

Request Parameters:

- **brandId**: Integer
- **productId**: Integer
- **dateTime**: LocalDateTime

**Response**:

Json of object **PriceRespondeDTO** :

```
PriceResponseDTO{ 
   brandId	integer($int32)
   productId	integer($int32)
   startDate	string($date-time)
   endDate	string($date-time)
   price	number 
   priceList	integer($int32)
   currency	string 
}
```

curl sample of local call is:

```
curl -X GET "http://localhost:8080/prices?brandId=1&dateTime=2021-05-09T20%3A37%3A33.581Z&productId=35455" -H "accept: */*"
```

Response Sample:

```
{
  "brandId": 1,
  "productId": 35455,
  "priceList": 1,
  "startDate": "2020-06-14T00:00:00",
  "endDate": "2020-12-31T23:59:59",
  "price": 35.5,
  "currency": "EUR"
}
```

#### POST operation is described below.

**Method**: POST

**URL**: <server url>/prices

Request Body: Json of object **PriceRequestDTO**

```
PriceRequestDTO{ 
  brandId   integer($int32)
  productID integer($int32)
  dateTime  string($date-time)
}
```

Response:
Json of object **PriceRespondeDTO**

```
PriceResponseDTO{ 
   brandId	integer($int32)
   productId	integer($int32)
   startDate	string($date-time)
   endDate	string($date-time)
   price	number 
   priceList	integer($int32)
   currency	string 
}
```

curl sample of local call is:

```
curl -X POST "http://localhost:8080/prices" -H "accept: */*" 
     -H "Content-Type: application/json" 
     -d "{ \"brandId\": 0, \"dateTime\": \"2021-05-09T20:37:33.601Z\", \"productID\": 0}"
```

### Data - Table prices

Find below described the data structure that stores information of this service

| Column Name | Description | Default Value | Data Type |
  |:---|:----|:---|:---|
|BRAND_ID | Foreign key of group brand | (1 = ZARA).| Integer
|START_DATE , END_DATE | application date of the price list | | DateTime
|PRICE_LIST | price list identifier | | Integer
|PRODUCT_ID | Product Identifier | | Integer
|PRIORITY | Priority of this price list row | | Integer
|PRICE | Final selling price | | Number(10,5)
|CURR | currency Iso | | char(3)

### Code Organization/Design

for convenience I built the service as one single module project. But usually we would have every package in a different
module with api, dto model, database access, entities and business logic separated.

So if we navigate to project root we will find:

- **src**: folder that contains source code and tests
- **Dockerfile**: file describing how to build a docker of this application
- **pom.xml**: maven file for dependency and build management

Folder **src** is subdivided in main (source code and classes) and tests (testing of the service).

I divided testing in 3 groups:

- **business cases**: the five cases of the java test contained in class BusinessCasesTest
- **Integration**: integration tests of the service. All integration test must extend AbstractTestBase that provides and
  easy method to invoke spring application context.
- **Unit**: unit testing of components

Folder **main** is divided in java and resources.

**resources** contains 2 files:

- **application.xml**: spring, jpa, h2, logging and service configuration. If we want to go live with this service we
  should study and provide a method of profile management (not included)
- **data.sql**: script that creates prices table and loads sample data

Folder **java** contains 6 packages and the main service class **PricesServiceApplication**

service packages are:

- **api**: this folder will contain all public controllers of the application
    - so far it has:
        - **PricesController**: main controller of the service "/prices"
        - **PricesCrudController**: included for manual local managing of data "/prices-operations"
- **config**: swagger configuration
- **dto**: contains the objects we will use to interact with the service
    - so far it has:
        - **PriceRequestDTO**: designed as request body of POST "/prices" operation
        - **PriceResponseDTO**: designed as response body of GET and POST "/prices" operation
- **entity**: JPA entities designed to interact with the database
    - **PricesDAO**: model object of prices table
- **repository**: package of JPA repository interfaces to interact with database
    - PricesRepository: extends JpaRepository and adds a method called **findPriceByBrandAndProductAndDate**
- **service**: package of business logic, this will contain any service that provide functionality to the system
    - **PricesService**: interface provided for dependency injection, contains just one method called getPrices
    - **PricesServiceImpl**: this class responsibilities are manage interaction between public controllers and the
      repository and mapping of DTO objects to Entities and reverse.

### Howto build and run

this service is bundled as a maven project in a gitlab repository.

Steps for building the service are:

- clone sources to local:

```
git clone https://gitlab.com/akakike/capitole.git
```

- get into sources folder and execute maven (tested with maven 3.6.3 in linux)

```
mvn clean install
```

- if we have docker tools installed we can build/run the docker image of the project with

```
docker build .
docker run <image id>
```

- if we want to run the application as java locally

```
java -jar target/prices-service-1.0.0.jar
```

- then we can test the application in port 8080 (java run) or port configured for docker if we want to find port
  assigned when docker run we can just execute

```  
  docker ps <image id>
```
package com.capitole.inditex.businessCases;

import com.capitole.inditex.api.PricesController;
import com.capitole.inditex.dto.PriceResponseDTO;
import com.capitole.inditex.exception.PriceNotFoundException;
import com.capitole.inditex.integration.AbstractTestBase;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
class BusinessCasesTest extends AbstractTestBase {

    @Autowired
    PricesController pricesController;

    private void callBusinessCaseAndTestResult(final LocalDateTime date,
                                               final Integer rqProductId,
                                               final Integer rqBrandId,
                                               final PriceResponseDTO expectedPriceResponseDTO) throws PriceNotFoundException {
        final var result = pricesController.getPrices(rqBrandId, rqProductId, date);

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(expectedPriceResponseDTO, result.getBody());


    }

    // - Test 1: petición a las 10:00 del día 14 del producto 35455 para la brand 1 (ZARA)
    @Test
    void test_1_when_rq_at_10_00_day_14_product_35455_brand_1_then_returns_price_35_50() throws PriceNotFoundException {
        final var requestTime = LocalDateTime.of(2020, 6, 14, 10, 0, 0);

        final var expectedStartDate = LocalDateTime.of(2020, 6, 14, 0, 0, 0);
        final var expectedEndDate = LocalDateTime.of(2020, 12, 31, 23, 59, 59);
        final var expectedResult = PriceResponseDTO.builder()
                .brandId(1)
                .productId(35455)
                .priceList(1)
                .startDate(expectedStartDate)
                .endDate(expectedEndDate)
                .price(BigDecimal.valueOf(35.50).setScale(5, RoundingMode.HALF_UP))
                .currency("EUR");
        callBusinessCaseAndTestResult(requestTime, 35455, 1, expectedResult.build());
    }

    //- Test 2: petición a las 16:00 del día 14 del producto 35455 para la brand 1 (ZARA)
    @Test
    void test_2_when_rq_at_16_00_day_14_product_35455_brand_1_then_returns_price_25_45() throws PriceNotFoundException {
        final var requestTime = LocalDateTime.of(2020, 6, 14, 16, 0, 0);

        final var expectedStartDate = LocalDateTime.of(2020, 6, 14, 15, 0, 0);
        final var expectedEndDate = LocalDateTime.of(2020, 6, 14, 18, 30, 0);
        final var expectedResult = PriceResponseDTO.builder()
                .brandId(1)
                .productId(35455)
                .priceList(2)
                .startDate(expectedStartDate)
                .endDate(expectedEndDate)
                .price(BigDecimal.valueOf(25.45).setScale(5, RoundingMode.HALF_UP))
                .currency("EUR");
        callBusinessCaseAndTestResult(requestTime, 35455, 1, expectedResult.build());
    }

    //- Test 3: petición a las 21:00 del día 14 del producto 35455 para la brand 1 (ZARA)
    @Test
    void test_3_when_rq_at_21_00_day_14_product_35455_brand_1_then_returns_price_35_50() throws PriceNotFoundException {
        final var requestTime = LocalDateTime.of(2020, 6, 14, 21, 0, 0);

        final var expectedStartDate = LocalDateTime.of(2020, 6, 14, 0, 0, 0);
        final var expectedEndDate = LocalDateTime.of(2020, 12, 31, 23, 59, 59);
        final var expectedResult = PriceResponseDTO.builder()
                .brandId(1)
                .productId(35455)
                .priceList(1)
                .startDate(expectedStartDate)
                .endDate(expectedEndDate)
                .price(BigDecimal.valueOf(35.50).setScale(5, RoundingMode.HALF_UP))
                .currency("EUR");

        callBusinessCaseAndTestResult(requestTime, 35455, 1, expectedResult.build());
    }


    //- Test 4: petición a las 10:00 del día 15 del producto 35455 para la brand 1 (ZARA)
    @Test
    void test_4_when_rq_at_10_00_day_15_product_35455_brand_1_then_returns_price_30_50() throws PriceNotFoundException {
        final var requestTime = LocalDateTime.of(2020, 6, 15, 10, 0, 0);

        final var expectedStartDate = LocalDateTime.of(2020, 6, 15, 0, 0, 0);
        final var expectedEndDate = LocalDateTime.of(2020, 6, 15, 11, 0, 0);
        final var expectedResult = PriceResponseDTO.builder()
                .brandId(1)
                .productId(35455)
                .priceList(3)
                .startDate(expectedStartDate)
                .endDate(expectedEndDate)
                .price(BigDecimal.valueOf(30.50).setScale(5, RoundingMode.HALF_UP))
                .currency("EUR");

        callBusinessCaseAndTestResult(requestTime, 35455, 1, expectedResult.build());
    }

    //- Test 5: petición a las 21:00 del día 16 del producto 35455 para la brand 1 (ZARA)
    @Test
    void test_5_when_rq_at_21_00_day_16_product_35455_brand_1_then_returns_price_38_95() throws PriceNotFoundException {
        final var requestTime = LocalDateTime.of(2020, 6, 16, 21, 0, 0);

        final var expectedStartDate = LocalDateTime.of(2020, 6, 15, 16, 0, 0);
        final var expectedEndDate = LocalDateTime.of(2020, 12, 31, 23, 59, 59);
        final var expectedResult = PriceResponseDTO.builder()
                .brandId(1)
                .productId(35455)
                .priceList(4)
                .startDate(expectedStartDate)
                .endDate(expectedEndDate)
                .price(BigDecimal.valueOf(38.95).setScale(5, RoundingMode.HALF_UP))
                .currency("EUR");

        callBusinessCaseAndTestResult(requestTime, 35455, 1, expectedResult.build());
    }

    @Test
    void test_when_rq_at_21_00_day_14_product_35455_brand_1_then_returns_PriceNotFoundException() throws PriceNotFoundException {
        final var requestTime = LocalDateTime.of(2020, 6, 16, 21, 0, 0);

        final var expectedStartDate = LocalDateTime.of(2020, 6, 15, 16, 0, 0);
        final var expectedEndDate = LocalDateTime.of(2020, 12, 31, 23, 59, 59);
        final var expectedResult = PriceResponseDTO.builder()
                .brandId(1)
                .productId(35455)
                .priceList(4)
                .startDate(expectedStartDate)
                .endDate(expectedEndDate)
                .price(BigDecimal.valueOf(38.95).setScale(5, RoundingMode.HALF_UP))
                .currency("EUR");

        callBusinessCaseAndTestResult(requestTime, 35455, 1, expectedResult.build());
    }
}

package com.capitole.inditex.unit.api;

import com.capitole.inditex.api.PricesController;
import com.capitole.inditex.dto.PriceRequestDTO;
import com.capitole.inditex.dto.PriceResponseDTO;
import com.capitole.inditex.exception.PriceNotFoundException;
import com.capitole.inditex.service.PricesServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PricesControllerUnitTest {

    @Mock
    PricesServiceImpl pricesService;

    PricesController pricesController;

    @BeforeEach
    public void setup() {
        pricesController = new PricesController(pricesService);
    }

    @Test
    void test_when_get_prices_call_service() throws PriceNotFoundException {
        final var rqDateTime = LocalDateTime.of(2020, 6, 14, 10, 0, 0);

        final var expectedStartDate = LocalDateTime.of(2020, 6, 14, 0, 0, 0);
        final var expectedEndDate = LocalDateTime.of(2020, 12, 31, 23, 59, 59);
        final PriceResponseDTO price = PriceResponseDTO.builder()
                .brandId(1)
                .productId(35455)
                .priceList(1)
                .startDate(expectedStartDate)
                .endDate(expectedEndDate)
                .price(BigDecimal.valueOf(35.50))
                .currency("EUR").build();

        when(pricesService.getPrices(1, 35545, rqDateTime)).thenReturn(price);

        final var responsePrice = pricesController.getPrices(1, 35545, rqDateTime);

        assertEquals(price, responsePrice.getBody());
    }

    @Test
    void test_when_post_prices_call_service() throws PriceNotFoundException {
        final var rqDateTime = LocalDateTime.of(2020, 6, 14, 10, 0, 0);
        final var pricesRequestDTO = PriceRequestDTO.builder().brandId(1).productID(35455).dateTime(rqDateTime);
        final var expectedStartDate = LocalDateTime.of(2020, 6, 14, 0, 0, 0);
        final var expectedEndDate = LocalDateTime.of(2020, 12, 31, 23, 59, 59);
        final PriceResponseDTO price = PriceResponseDTO.builder()
                .brandId(1)
                .productId(35455)
                .priceList(1)
                .startDate(expectedStartDate)
                .endDate(expectedEndDate)
                .price(BigDecimal.valueOf(35.50))
                .currency("EUR").build();

        when(pricesService.getPrices(1, 35455, rqDateTime)).thenReturn(price);

        final var responsePrice = pricesController.postPrices(pricesRequestDTO.build());

        assertEquals(price, responsePrice.getBody());
    }
}

package com.capitole.inditex.unit.service;

import com.capitole.inditex.dto.PriceResponseDTO;
import com.capitole.inditex.entity.PricesDAO;
import com.capitole.inditex.exception.PriceNotFoundException;
import com.capitole.inditex.repository.PricesRepository;
import com.capitole.inditex.service.PricesService;
import com.capitole.inditex.service.PricesServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PricesServiceImplUnitTest {

    @Mock
    PricesRepository pricesRepository;

    PricesService pricesService;

    @BeforeEach
    public void setup() {
        pricesService = new PricesServiceImpl(pricesRepository);
    }

    @Test
    void test_when_get_prices_then_call_repository_and_return_dto() throws PriceNotFoundException {
        final var dateTime = LocalDateTime.of(2020, 6, 14, 10, 0, 0);
        final var expectedStartDate = LocalDateTime.of(2020, 6, 14, 0, 0, 0);
        final var expectedEndDate = LocalDateTime.of(2020, 12, 31, 23, 59, 59);
        final var priceDAO = PricesDAO.builder()
                .id(1L)
                .brandId(1)
                .productId(35455)
                .priceList(1)
                .startDate(expectedStartDate)
                .endDate(expectedEndDate)
                .price(BigDecimal.valueOf(35.5).setScale(5, RoundingMode.HALF_UP))
                .currency("EUR");

        when(pricesRepository.findPriceByBrandAndProductAndDate(1, 35455, dateTime)).thenReturn(priceDAO.build());

        final var result = pricesService.getPrices(1, 35455, dateTime);

        final var expectedPrice = PriceResponseDTO.builder()
                .brandId(1)
                .productId(35455)
                .priceList(1)
                .startDate(expectedStartDate)
                .endDate(expectedEndDate)
                .price(BigDecimal.valueOf(35.5).setScale(5, RoundingMode.HALF_UP))
                .currency("EUR");

        assertEquals(expectedPrice.build(), result);

    }

    @Test()
    void test_when_get_prices_then_call_repository_if_null_PriceNotFoundException() {
        final var dateTime = LocalDateTime.of(2020, 6, 14, 10, 0, 0);

        when(pricesRepository.findPriceByBrandAndProductAndDate(1, 35455, dateTime)).thenReturn(null);

        assertThrows(PriceNotFoundException.class, () -> pricesService.getPrices(1, 35455, dateTime));

    }
}

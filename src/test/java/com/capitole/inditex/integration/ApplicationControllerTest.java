package com.capitole.inditex.integration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ApplicationControllerTest extends AbstractTestBase {

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();
    }

    @Test
    void testApplicationSwagger() throws Exception {
        final String uri = "/swagger-ui.html";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
    }
}
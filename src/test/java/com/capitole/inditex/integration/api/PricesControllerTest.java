package com.capitole.inditex.integration.api;

import com.capitole.inditex.dto.PriceRequestDTO;
import com.capitole.inditex.dto.PriceResponseDTO;
import com.capitole.inditex.integration.AbstractTestBase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PricesControllerTest extends AbstractTestBase {

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
    }

    @Test
    void testPricesController_when_called_returns_ok() throws Exception {
        assertEquals(200, getMockHttpServletResponse(
                "/prices?brandId=1&dateTime=2020-06-22T17:38:00&productId=35455").getStatus());
    }

    @Test
    void testPricesController_when_called_returns_not_found() throws Exception {
        assertEquals(404, getMockHttpServletResponse(
                "/prices?brandId=2&dateTime=2020-06-22T17:38:00&productId=35455").getStatus());
    }

    @Test
    void testPricesController_when_called_with_bad_format_returns_bad_request() throws Exception {
        assertEquals(400, getMockHttpServletResponse(
                "/prices?brandId=2&dateTime=202006-22T17:38:00&productId=35455").getStatus());
    }

    @Test
    void testPricesController_when_called_without_parameter_returns_bad_request() throws Exception {
        assertEquals(400, getMockHttpServletResponse("/prices").getStatus());
    }

    @Test
    void testPricesController_when_called_by_post_returns_ok() throws Exception {
        var priceRequestDTO = PriceRequestDTO.builder()
                .brandId(1)
                .productID(35455)
                .dateTime(LocalDateTime.of(2020, 6, 15, 10, 0, 0));

        MockHttpServletResponse response = getMockHttpServletResponse("/prices", priceRequestDTO.build());

        assertEquals(200, response.getStatus());
        assertEquals(BigDecimal.valueOf(30.50).setScale(5, RoundingMode.HALF_UP), mapFromJson(response.getContentAsString(), PriceResponseDTO.class).getPrice());
    }

    @Test
    void testPricesController_when_called_by_post_returns_not_found() throws Exception {
        var priceRequestDTO = PriceRequestDTO.builder()
                .brandId(2)
                .productID(35455)
                .dateTime(LocalDateTime.of(2020, 6, 15, 10, 0, 0));
        MockHttpServletResponse response = getMockHttpServletResponse("/prices", priceRequestDTO.build());

        assertEquals(404, response.getStatus());
    }

    @Test
    void testPricesController_when_called_by_post_returns_bad_request() throws Exception {
        assertEquals(400, getMockHttpServletResponse("/prices", "").getStatus());
    }

    private MockHttpServletResponse getMockHttpServletResponse(String uri, Object requestBody) throws Exception {
        return mvc.perform(MockMvcRequestBuilders.post(uri).content(mapToJson(requestBody))
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn().getResponse();
    }

    private MockHttpServletResponse getMockHttpServletResponse(String uri) throws Exception {
        return mvc.perform(MockMvcRequestBuilders.get(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn().getResponse();
    }
}
package com.capitole.inditex.integration.repository;

import com.capitole.inditex.entity.PricesDAO;
import com.capitole.inditex.integration.AbstractTestBase;
import com.capitole.inditex.repository.PricesRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class PricesRepositoryTest extends AbstractTestBase {

    @Autowired
    PricesRepository pricesRepository;

    @Transactional
    @Test
    void testFindById() {
        var price = pricesRepository.getOne(2L);
        assertEquals(1, price.getPriority());
    }


    @Transactional
    @Test
    void testFindPriceByBrandAndProductAndDate() {
        var price = pricesRepository.findPriceByBrandAndProductAndDate(
                1,
                35455,
                LocalDateTime.of(2020, 6, 14, 10, 0, 0));

        var expectedResult = PricesDAO.builder()
                .id(1L)
                .brandId(1)
                .productId(35455)
                .priceList(1)
                .startDate(LocalDateTime.of(2020, 6, 14, 0, 0, 0))
                .endDate(LocalDateTime.of(2020, 12, 31, 23, 59, 59))
                .price(BigDecimal.valueOf(35.5).setScale(5, RoundingMode.HALF_UP))
                .currency("EUR");

        assertEquals(expectedResult.build(), price);
    }

    @Transactional
    @Test
    void testFindPriceByBrandAndProductAndDatePreviousNoResult() {
        var price = pricesRepository.findPriceByBrandAndProductAndDate(
                1,
                35455,
                LocalDateTime.of(2020, 6, 13, 23, 59, 59));

        assertNull(price);
    }

    @Transactional
    @Test
    void testFindPriceByBrandAndProductAndDateAfterNoResult() {
        var price = pricesRepository.findPriceByBrandAndProductAndDate(
                1,
                35455,
                LocalDateTime.of(2021, 1, 1, 0, 0, 0));

        assertNull(price);
    }
}
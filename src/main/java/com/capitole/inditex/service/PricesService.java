package com.capitole.inditex.service;

import com.capitole.inditex.dto.PriceResponseDTO;
import com.capitole.inditex.exception.PriceNotFoundException;

import java.time.LocalDateTime;

public interface PricesService {
    PriceResponseDTO getPrices(Integer brandId, Integer productId, LocalDateTime dateTime) throws PriceNotFoundException;
}

package com.capitole.inditex.service;

import com.capitole.inditex.dto.PriceResponseDTO;
import com.capitole.inditex.entity.PricesDAO;
import com.capitole.inditex.exception.PriceNotFoundException;
import com.capitole.inditex.repository.PricesRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class PricesServiceImpl implements PricesService {

    private final PricesRepository pricesRepository;

    @Autowired
    public PricesServiceImpl(final PricesRepository pricesRepository) {
        this.pricesRepository = pricesRepository;
    }

    @Override
    public PriceResponseDTO getPrices(final Integer brandId, final Integer productId, final LocalDateTime dateTime) throws PriceNotFoundException {
        final var priceByBrandAndProductAndDate = pricesRepository.findPriceByBrandAndProductAndDate(brandId, productId, dateTime);
        if (priceByBrandAndProductAndDate == null) {
            throw new PriceNotFoundException();
        }
        return mapResult(priceByBrandAndProductAndDate);
    }

    private PriceResponseDTO mapResult(final PricesDAO priceByBrandAndProductAndDate) {
        final var priceResponseDTO = new PriceResponseDTO();
        BeanUtils.copyProperties(priceByBrandAndProductAndDate, priceResponseDTO);
        return priceResponseDTO;
    }
}
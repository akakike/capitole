package com.capitole.inditex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
public class PricesServiceApplication {
    public static void main(String[] args) {
        var app = new SpringApplication(PricesServiceApplication.class);
        app.run(args);
    }
}

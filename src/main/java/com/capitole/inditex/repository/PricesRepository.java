package com.capitole.inditex.repository;

import com.capitole.inditex.entity.PricesDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;

public interface PricesRepository extends JpaRepository<PricesDAO, Long> {

    @Query(value = "SELECT id,brand_Id,start_Date,end_Date,price_List,product_Id,priority,price,curr " +
            "FROM Prices p " +
            "WHERE p.brand_Id = :brandId " +
            "  AND p.product_Id = :productId " +
            "  AND start_Date <= :dateTime AND end_Date > :dateTime " +
            " ORDER BY priority DESC " +
            " LIMIT 1",
            nativeQuery = true)
    PricesDAO findPriceByBrandAndProductAndDate(
            @Param("brandId") Integer brandId,
            @Param("productId") Integer productId,
            @Param("dateTime") LocalDateTime dateTime
    );
}

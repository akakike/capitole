package com.capitole.inditex.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PriceRequestDTO {
    Integer brandId;
    Integer productID;
    LocalDateTime dateTime;
}

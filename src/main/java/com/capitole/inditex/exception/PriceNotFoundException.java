package com.capitole.inditex.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class PriceNotFoundException extends Exception {
    private static final long serialVersionUID = 3608727059844451511L;
}

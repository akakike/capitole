package com.capitole.inditex.api;

import com.capitole.inditex.entity.PricesDAO;
import com.capitole.inditex.repository.PricesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * This class is built just for testing purposes, DO NOT USE IT in productive environments
 */
@RequestMapping("/prices-operations")
@RestController
@ConditionalOnProperty(value = "prices-service.enableCrud", havingValue = "true")
public class PricesCrudController {

    private final PricesRepository pricesRepository;

    @Autowired
    public PricesCrudController(PricesRepository pricesRepository) {
        this.pricesRepository = pricesRepository;
    }

    @GetMapping("/{id}")
    public ResponseEntity<PricesDAO> getPrice(
            @PathVariable Long id
    ) {
        return pricesRepository.existsById(id) ?
                ResponseEntity.status(HttpStatus.OK).body(pricesRepository.findById(id).orElse(null)) :
                ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @GetMapping
    public ResponseEntity<List<PricesDAO>> findPrices() {
        return ResponseEntity.status(HttpStatus.OK).body(pricesRepository.findAll());
    }

    @SuppressWarnings("java:S4684")
    @PostMapping
    public ResponseEntity<PricesDAO> insertPrice(
            @RequestBody PricesDAO pricesDAO
    ) {
        return (!pricesRepository.existsById(pricesDAO.getId())) ?
                ResponseEntity.status(HttpStatus.OK).body(pricesRepository.save(pricesDAO)) :
                ResponseEntity.status(HttpStatus.CONFLICT).build();
    }

    @SuppressWarnings("java:S4684")
    @PutMapping
    public ResponseEntity<PricesDAO> updatePrice(
            @RequestBody PricesDAO pricesDAO
    ) {
        return pricesRepository.existsById(pricesDAO.getId()) ?
                ResponseEntity.status(HttpStatus.OK).body(pricesRepository.save(pricesDAO)) :
                ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @DeleteMapping("/{id}")
    public HttpStatus deletePrice(@PathVariable Long id
    ) {
        if (pricesRepository.existsById(id)) {
            pricesRepository.deleteById(id);
            return HttpStatus.OK;
        } else {
            return HttpStatus.NOT_FOUND;
        }
    }
}

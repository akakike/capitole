package com.capitole.inditex.api;

import com.capitole.inditex.dto.PriceRequestDTO;
import com.capitole.inditex.dto.PriceResponseDTO;
import com.capitole.inditex.exception.PriceNotFoundException;
import com.capitole.inditex.service.PricesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RequestMapping("/prices")
@RestController
public class PricesController {

    private final PricesService pricesService;

    @Autowired
    public PricesController(final PricesService pricesService) {
        this.pricesService = pricesService;
    }

    @GetMapping
    public ResponseEntity<PriceResponseDTO> getPrices(
            @RequestParam final Integer brandId,
            @RequestParam final Integer productId,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final LocalDateTime dateTime
    ) throws PriceNotFoundException {
        return ResponseEntity.status(HttpStatus.OK).body(pricesService.getPrices(brandId, productId, dateTime));
    }

    @PostMapping
    public ResponseEntity<PriceResponseDTO> postPrices(
            @RequestBody final PriceRequestDTO priceRequestDTO
    ) throws PriceNotFoundException {
        return ResponseEntity.status(HttpStatus.OK).body(pricesService.getPrices(priceRequestDTO.getBrandId(), priceRequestDTO.getProductID(), priceRequestDTO.getDateTime()));
    }
}
